// метод объекта - это функция для получения доступа к объекту и его свойствам, инстурмент для модификации и взаимодействия с объектом
function createNewUser(firstName, lastName) {
    const newUser = {
      getLogin: function() {
        return "login: " + this.firstName.trim().charAt(0).toLowerCase() + this.lastName.trim().toLowerCase()   //.trim для того чтоб избежать ошибок с пробелами
      }, 
      setFirstName: function(newFirstName) {
        Object.defineProperties(newUser, {
          "firstName": {
            value: newFirstName
          }
        })
      },
      setLastName: function(newLastName) {
        Object.defineProperties(newUser, {
          "lastName": {
            value: newLastName
          }
        })
      }
    }
    Object.defineProperties(newUser, {
      "firstName": {
        value: firstName,
        enumerable: true,
        writable: false,
        configurable: true
      },
      "lastName": {
        value: lastName,
        enumerable: true,
        writable: false,
        configurable: true
      },
    })
    newUser.firstName = "TEST_NAME";
    newUser.lastName = "TEST_LASTNAME"; 
    // newUser.setFirstName("George")
    // newUser.setLastName("Harrison") //- проверка на смену значений 
    console.log(newUser)
    console.log(newUser.getLogin())
    return newUser
  }
  createNewUser(prompt("enter your name"), prompt("enter your last name"))